<?php
// Project Name: Milestone6
// Project Version: 1.5
// Module Name: List of Blog Posts With Update and Delete Blog Module
// Module Version: 1.5
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the update and delete functionality of the website as well as statistics
// References: https://www.w3schools.com/php/php_mysql_insert.asp
// https://www.w3schools.com/sql/sql_delete.asp
// --NOTE--
// Post title is <=100 characters
// Post text is <= 2000 characters
// A chat filter is also added for some inappropriate words

require_once('myfuncs.php');
$link = dbConnect();
    
$people = $_POST['person'];


foreach($people as $person)
{
    $id = $person["ID"];
    $firstname = $person["First Name"];
    $lastname = $person["Last Name"];
    $email = $person["Email"];
    $permissionLevel = $person["Permission Level"];

    // Check for empty input, otherwise exit out with an error
    if ($firstname == NULL) { $message = "The First Name or is a required field and cannot be blank.\n"; include('./loginFailed.php');      return; }
    if ($lastname == NULL)  { $message = "The Last Name is a required field and cannot be blank.\n"; include('./loginFailed.php');          return; }
    if ($email == NULL)     { $message = "The email is a required field and cannot be blank.\n"; include('./loginFailed.php');              return; }
    if ($permissionLevel == NULL)  { $message = "The permission level is a required field and cannot be blank.\n"; include('./loginFailed.php');           return; }

    // Check length
    if ($permissionLevel > 2)   die ("Cannot give permission higher than 2");
    if ($permissionLevel < 0)   die ("Cannot give permission lower than 0");

    // Check for illegal characters
    $illegal = array("'", "\"", "/", "\\", "[", "]", "{", "}", "(", ")");
    foreach ($illegal as $i) {
        if (strpos($firstname, $i) !== false) {
            echo $i;
            die ("ERROR: The username has one of the illegal characters: ' \" / \ [ ] ( ) { }\n");
        }
    }

    foreach ($illegal as $i) {
        if (strpos($lastname, $i) !== false) {
            die ("ERROR: The password has one of the illegal characters: ' \" / \ [ ] ( ) { }\n");
        }
    }

    $sql = "UPDATE users SET FIRST_NAME='$firstname', LAST_NAME='$lastname', EMAIL='$email', PERMISSION_LEVEL='$permissionLevel' WHERE ID='$id'";
    if (!$result = mysqli_query($link, $sql))
    {
        die("ERROR: %s\n" . mysqli_error($link));
    }
}
echo 'Succesfully updated permissions and information!';

mysqli_close($link);
?>

<a href="index.html">Return to main menu.</a>