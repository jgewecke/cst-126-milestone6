<a href="index.html">Go to main menu.</a>

<?php
// Project Name: Milestone6
// Project Version: 1.5
// Module Name: List of Blog Posts With Update and Delete Blog Module
// Module Version: 1.5
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the update and delete functionality of the website as well as statistics
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once './myfuncs.php';

$userID = getUserId();

// Check if user is signed in
if (is_null($userID))
{
    $message = "You must be logged in to do this.";
    include('./loginFailed.php');
    exit;
}

// Connect to db
$link = dbConnect();

$sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD FROM users WHERE ID='$userID'";

$result = mysqli_query($link, $sql);

$name = "NULL";
$row = $result->fetch_assoc();	// Read the Row from the Query
$name = $row["FIRST_NAME"];
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>Create a Blog Post</title>
</head>

<body>
     <h2>Hello <?php echo " " . $name; ?></h2>    
     <h1>Create a Blog Post</h1>
     <form action="submitPost.php" method="POST">
     <label>Blog Title</label>
     	<div class="group">
			<input type="text" name="Title"/>
		</div>
		<div class="group">
			<textarea id="textInput" name="TextInput" rows="4" cols="50"></textarea>
		</div>
		<input type="submit" value="Submit">
     </form>
    <h3>---Note---</h3>
	<p>The title of your post cannot contain more than 100 characters </p>
	<p>A blog post cannot contain more than 2000 characters </p>
</body>

</html>
