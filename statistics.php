<?php
// Project Name: Milestone6
// Project Version: 1.5
// Module Name: List of Blog Posts With Update and Delete Blog Module
// Module Version: 1.5
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the update and delete functionality of the website as well as statistics
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once('myfuncs.php');

function getNumberOfUsers($link)
{
    $sql = "SELECT ID FROM users";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfPosts($link)
{
    $sql = "SELECT ID FROM posts";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfGuests($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=0";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfModerators($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=1";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfAdmins($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=2";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

?>