<?php
// Project Name: Milestone6
// Project Version: 1.5
// Module Name: List of Blog Posts With Update and Delete Blog Module
// Module Version: 1.5
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the update and delete functionality of the website as well as statistics
// References: https://www.w3schools.com/php/php_mysql_insert.asp

function dbConnect() {
    // Connect to azure
    $link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");
    
    // Connect to local
    //$link = mysqli_connect("127.0.0.1", root, root, "activity1");
    
    // Check connection
    if($link === false){
        die("ERROR (myfuncs.php): Could not connect. " . mysqli_connect_error());
    }
    return $link;
}

function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
    $_SESSION["POST_ID"] = 0;
}
function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}

// Uses session to automatically get userID
function getUserArrayFromCurrentUser($link)
{
    session_start();
    $userID = $_SESSION["USER_ID"];
    
    $sql = "SELECT * FROM users WHERE ID='$userID'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if ($numRows >= 1)
    {
        return $result->fetch_assoc();	// Read the Row from the Query
    }
    else
    {
        echo('(myfuncs.php) No user found in database');
    }
}

function getPostFromID($link, $id)
{   
    $sql = "SELECT * FROM posts WHERE ID='$id'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if ($numRows >= 1)
    {
        return $result->fetch_assoc();	// Read the Row from the Query
    }
    else
    {
        echo('(myfuncs.php) No post found in database');
    }
}

?>