<link rel = "stylesheet" type = "text/css" href = "./style.css">

<?php
// Project Name: Milestone6
// Project Version: 1.5
// Module Name: List of Blog Posts With Update and Delete Blog Module
// Module Version: 1.5
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the update and delete functionality of the website as well as statistics

require_once './myfuncs.php';
require_once './statistics.php';

$link = dbConnect();

$userID = getUserId();



// Check if user is signed in
if (is_null($userID))
{
    $message = "You must be logged in to do this.";
    include('./loginFailed.php');
    exit;
}

// Check if user is admin
if (getUserArrayFromCurrentUser($link)["PERMISSION_LEVEL"] < 2)
{
    $message = "You must be an Admin to access this.";
    include('./loginFailed.php');
    exit;
}

$sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, PERMISSION_LEVEL FROM users";
$result = mysqli_query($link, $sql);
$numRows = mysqli_num_rows($result);

echo nl2br('<form action="adminHandler.php" method="POST">');
if ($numRows >= 1)
{
    echo nl2br('<h1>--- Edit Users ---</h1>');
    foreach($result as $index => $i) {
        $sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, PERMISSION_LEVEL FROM users";
        
        $id = $i["ID"];
        $firstName = $i["FIRST_NAME"];
        $lastName = $i["LAST_NAME"];
        $email = $i["EMAIL"];
        $permissionLevel = $i["PERMISSION_LEVEL"];
        
        // Display result
        echo nl2br('<div class="settings">');
        echo nl2br('<input type="hidden" name="person['.$index.'][ID]" value="'.$id.'">');
        echo nl2br('<label>First Name</label><input type="text" name="person['.$index.'][First Name]" value="'.$firstName.'">');
        echo nl2br('<label>Last Name</label><input type="text" name="person['.$index.'][Last Name]" value="'.$lastName.'">');
        echo nl2br('<label>Email</label><input type="text" name="person['.$index.'][Email]" value="'.$email.'">');
        echo nl2br('<label>Permission Level</label><input type="text" name="person['.$index.'][Permission Level]" value="'.$permissionLevel.'">');
        echo nl2br('</div>');
        echo nl2br('<br>');
    }
    echo nl2br('<br>');
}
echo nl2br('<input type="submit" value="Submit Changes">');
echo nl2br('</form>');
echo nl2br('<h1>--- Statistics ---</h1>');
echo "Total Users: " . getNumberOfUsers($link);
echo nl2br('<br>');
echo "-Number of Guests: " . getNumberOfGuests($link);
echo nl2br('<br>');
echo "-Number of Moderators: " . getNumberOfModerators($link);
echo nl2br('<br>');
echo "-Number of Admins: " . getNumberOfAdmins($link);
echo nl2br('<br>');
echo "Number of Posts: " . getNumberOfPosts($link);
echo nl2br('<br>');
?>

<a href="index.html">Return to main menu.</a>